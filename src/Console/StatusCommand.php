<?php

namespace Ox\Console;

use Ox\Config\DeployConfig;
use Ox\Service\BundleManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class BrowseCommand
 *
 * @package Ox\Console
 */
class StatusCommand extends Command
{
    protected static $defaultName = 'status';

    private DeployConfig  $config;
    private BundleManager $bundleManager;

    public function __construct(DeployConfig $config, ?BundleManager $bundleManager)
    {
        parent::__construct();

        $this->config        = $config;
        $this->bundleManager = $bundleManager;
    }

    protected function configure(): void
    {
        $this->setDescription('Reports the current bundle status if available');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        if (!$this->bundleManager->hasCurrentData()) {
            $io->error('Current bundle data file is missing : ' . $this->bundleManager->getStatusFilePath());
            return 1;
        }

        $table = new Table($output);
        $table->setHeaderTitle('Current bundle status');
        $table->setHeaders(
            [
                'Unique Id',
                'Release Code',
                'Last Update',
            ]
        );

        $table->addRow([
            $this->bundleManager->getCurrentUniqueIdentifier(),
            $this->bundleManager->getCurrentReleaseCode(),
            $this->bundleManager->getCurrentLatestUpdateDate(),
        ]);

        $table->render();

        return 0;
    }
}
