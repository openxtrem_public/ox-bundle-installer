<?php

namespace Ox\Console;

use Exception;
use Ox\Config\DeployConfig;
use Ox\Console\Traits\StopwatchCommandTrait;
use Ox\Service\BundleManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class BuildCommand
 *
 * @package Ox\Console
 */
class BuildCommand extends Command
{
    use StopwatchCommandTrait;

    protected static $defaultName = 'build';

    private DeployConfig  $config;
    private BundleManager $bundleManager;

    public function __construct(DeployConfig $config, BundleManager $bundleManager)
    {
        parent::__construct();
        $this->config        = $config;
        $this->bundleManager = $bundleManager;
    }

    protected function configure(): void
    {
        $this->setDescription('Build bundle dependencies and assets');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('Building application dependencies and assets...');

        $this->startWatch($this->getName());

        $this->bundleManager->build();

        $io->success(
            'Dependencies were installed in '
            . $this->getWatchReadableDuration($this->getName())
        );

        return 0;
    }
}
