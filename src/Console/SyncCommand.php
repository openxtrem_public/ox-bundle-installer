<?php

namespace Ox\Console;

use Exception;
use Ox\Config\DeployConfig;
use Ox\Console\Traits\StopwatchCommandTrait;
use Ox\Entity\Bundle;
use Ox\Service\BundleManager;
use Ox\Service\ERPManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class SyncCommand
 *
 * @package Ox\Console
 */
class SyncCommand extends Command
{
    use StopwatchCommandTrait;

    protected static $defaultName = 'sync';

    private BundleManager $bundleManager;
    private DeployConfig  $config;
    private ERPManager    $erpManager;

    public function __construct(DeployConfig $config, BundleManager $bundleManager, ERPManager $erpManager)
    {
        parent::__construct();
        $this->config        = $config;
        $this->bundleManager = $bundleManager;
        $this->erpManager    = $erpManager;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Synchronize bundle contents to local master directory')
            ->setDefinition(
                new InputDefinition([
                    new InputOption(
                        'auto',
                        null,
                        InputOption::VALUE_NONE,
                        'Non-interactive synchronization. Default usage with the auto-update command'
                    ),
                ])
            );
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws Exception|TransportExceptionInterface
     * @throws DecodingExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $auto = $input->getOption('auto');

        $io = new SymfonyStyle($input, $output);

        $uuid = $auto ?
            $this->config->getBundleUniqueId() :
            $io->ask('Enter the target bundle unique id.', $this->config->getBundleUniqueId());

        if (empty($uuid)) {
            $io->error('No bundle uuid was provided.');
            return 1;
        }

        $io->title('Checking bundle data from ERP...');

        $bundle = Bundle::createFromJSON($this->erpManager->getBundle($uuid));

        if (!($bundle instanceof Bundle)) {
            $io->error('No bundle match this unique id.');
            return 1;
        }

        $io->definitionList(
            ['uuid'           => $bundle->getUUID()],
            ['instance_id'    => $bundle->getInstanceId()],
            ['label'          => $bundle->getLabel()],
            ['integrity'      => $bundle->getIntegrity()],
            ['branch'         => $bundle->getBranch()],
            ['last_sync_date' => $bundle->getLastSyncDate() . ' (' . $bundle->getLastSyncRelativeDate() . ')'],
        );

        foreach ($this->config->getInstancesIdentifiers() as $identifier) {
            if ($identifier !== $bundle->getInstanceId()) {
                $io->caution(
                    [
                        'Bundle is dedicated to instance id : ' . $bundle->getInstanceId() . '.',
                        'Invalid instance id was found in config: ' . $identifier . '.',
                        'Check the configuration. Aborting install...'
                    ]
                );
                return 1;
            }
        }

        if (!$bundle->isActive() || !$bundle->getIntegrity()) {
            $io->warning(
                [
                'Bundle is inactive or its integrity is compromised.',
                'Consider checking the bundle status on OX-ERP.',
                'Aborting install...'
                ]
            );
            return 1;
        }

        $cautions = [
            'release_code' => [
                'target'  => $bundle->getBranch(),
                'current' => $this->bundleManager->getCurrentReleaseCode(),
            ],
            'uuid' => [
                'target'  => $bundle->getUUID(),
                'current' => $this->bundleManager->getCurrentUniqueIdentifier(),
            ],
        ];

        foreach ($cautions as $name => $caution) {
            if (!empty($caution['current']) && ($caution['target'] !== $caution['current'])) {
                $io->warning(
                    [
                        'Current bundle ' . $name . ' (' . $caution['current'] .')',
                        'Target bundle ' . $name . ' (' . $caution['target'] . ').',
                        'Aborting process if auto-updating...',
                    ]
                );

                if ($auto) {
                    $io->error('Aborting process on auto-update.');
                    return 1;
                }
            }
        }

        if (!$auto && !$io->confirm('Proceed to synchronization ?', false)) {
            $io->note('Synchronization canceled.');
            return 1;
        }

        $io->title('Synchronizing bundle contents to local master directory...');

        $this->startWatch($this->getName());

        $this->bundleManager->sync($bundle);
        $io->success(
            'Bundle synchronized successfully to master directory at : '
            . $this->config->getMasterDirectory()
            . ' in ' . $this->getWatchReadableDuration($this->getName())
        );

        if (!$this->erpManager->markBundleSynchronized($bundle->getUUID())) {
            $io->warning('Sending synchronization feedback to ERP has failed.');
        }

        return 0;
    }
}
