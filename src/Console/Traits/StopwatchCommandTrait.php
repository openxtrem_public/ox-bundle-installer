<?php

namespace Ox\Console\Traits;

use Carbon\Carbon;
use Carbon\CarbonInterface;
use Symfony\Component\Stopwatch\Stopwatch;

trait StopwatchCommandTrait
{
    private Stopwatch $stopwatch;

    public function startWatch(string $name): void
    {
        $this->stopwatch = new Stopwatch();
        $this->stopwatch->start($name);
    }

    /**
     * Returns the event with $name duration in floating seconds
     *
     * @param string $name
     *
     * @return string|null
     */
    public function getWatchReadableDuration(string $name): ?string
    {
        if ($this->stopwatch->isStarted($name)) {
            $this->stopwatch->stop($name);

            return Carbon::now()
                ->addMilliseconds($this->stopwatch->getEvent($name)->getDuration())
                ->diffForHumans(null, CarbonInterface::DIFF_ABSOLUTE, false, 2);
        }

        return null;
    }
}