<?php

namespace Ox\Console;

use Ox\Config\DeployConfig;
use Ox\Entity\Bundle;
use Ox\Service\BundleManager;
use Ox\Service\ERPManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class BrowseCommand
 *
 * @package Ox\Console
 */
class BrowseCommand extends Command
{
    protected static $defaultName = 'browse';

    private DeployConfig $config;
    private ERPManager $erpManager;

    public function __construct(DeployConfig $config, ?BundleManager $bundleManager, ERPManager $erpManager)
    {
        parent::__construct();

        $this->config     = $config;
        $this->erpManager = $erpManager;
    }

    protected function configure(): void
    {
        $this->setDescription('Browse and display available bundles');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $bundles = Bundle::createArrayFromJSON($this->erpManager->listBundles());

        $table = new Table($output);
        $table->setHeaderTitle('Available bundles list from ERP (' . count($bundles) . ')');
        $table->setHeaders(
            [
                'Unique Id',
                'Label',
                'Active',
            ]
        );

        foreach ($bundles as $bundle) {
            $table->addRow([
                $bundle->getUUID(),
                $bundle->getLabel(),
                $bundle->isActive(),
            ]);
        }

        $table->render();

        return 0;
    }
}
