<?php

namespace Ox\Console;

use Ox\Config\DeployConfig;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class ConfigCommand
 *
 * @package Ox\Console
 */
class ConfigCommand extends Command
{
    protected static $defaultName = 'config';

    private DeployConfig $config;

    public function __construct(DeployConfig $config)
    {
        parent::__construct();
        $this->config = $config;
    }

    protected function configure(): void
    {
        $this->setDescription('Load and validate configuration file');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->config->checkBinaryRequirements();

        $io->success('Binary requirements are valid.');

        $violations = $this->config->getViolationsList();

        if ($violations->count() > 0) {
            $io->error('Configuration is invalid.');
            $table = new Table($output);
            $table->setHeaderTitle('Configuration violations (' . $violations->count() . ')');
            $table->setHeaders(
                [
                    'Parameter',
                    'Reason',
                    'Invalid value',
                ]
            );

            foreach ($violations as $violation) {
                $invalidValue = $violation->getInvalidValue();
                if (is_object($invalidValue) || is_array($invalidValue)) {
                    $invalidValue = json_encode($invalidValue);
                }

                $table->addRow([
                    $violation->getPropertyPath(),
                    $violation->getMessage(),
                    $invalidValue
                ]);
            }

            $table->render();

            return 1;

        } else {
            $io->success('Configuration loaded and validated successfully.');
        }

        return 0;
    }
}
