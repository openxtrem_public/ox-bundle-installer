<?php

namespace Ox\Console;

use Ox\Config\DeployConfig;
use Ox\Console\Traits\StopwatchCommandTrait;
use Ox\Entity\Server;
use Ox\Service\BundleManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class DeployCommand
 *
 * @package Ox\Console
 */
class DeployCommand extends Command
{
    use StopwatchCommandTrait;

    protected const ALL_SERVERS = 'ALL';

    protected static $defaultName = 'deploy';

    /** @var DeployConfig  */
    private DeployConfig $config;
    private BundleManager $bundleManager;
    private SymfonyStyle $io;

    public function __construct(DeployConfig $config, BundleManager $bundleManager)
    {
        parent::__construct();
        $this->config        = $config;
        $this->bundleManager = $bundleManager;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Deploy bundle from master directory to target servers directories')
            ->setDefinition(
                new InputDefinition([
                    new InputArgument(
                        'instances',
                        InputArgument::IS_ARRAY,
                        'Non-interactive deployment with a list of authorized instance identifiers'
                    ),
                    new InputOption(
                        'dry-run',
                        null,
                        InputOption::VALUE_NONE,
                        'Dry-run mode. Display the list of files diff.'
                    )
                ])
            );
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $dryRun       = $input->getOption('dry-run');
        $instancesIds = $input->getArgument('instances');
        $auto         = !empty($instancesIds);

        $this->io = new SymfonyStyle($input, $output);

        $this->io->title('Preparing for deployment...');

        $servers = $this->config->getServers();

        if (empty($servers)) {
            $this->io->error('No servers configured for deployment');
            return 1;
        }

        if (true === $dryRun) {
            $this->io->note('Dry-run mode is on : no copy will be performed.');
        }

        $serverChoice = false;
        if ($auto) {
            $servers = Server::filterByInstances($servers, $instancesIds);
            if (empty($servers)) {
                $this->io->error('No servers allowed for deployment');
                return 1;
            }
            $serversCount = count($servers);
        } else {
            $serverChoice = $this->promptServerChoice($output, $servers);
            $serversCount = ($serverChoice === self::ALL_SERVERS) ? count($servers) : 1;
        }

        $this->io->title('Deploying...');

        $this->io->progressStart($serversCount);

        $this->startWatch($this->getName());

        foreach ($servers as $server) {
            if (!$auto && !in_array($serverChoice, [self::ALL_SERVERS, $server->getName()])) {
                continue;
            }

            $this->bundleManager->deploy($server, $dryRun);
            $this->bundleManager->purge($server, $dryRun);
            $this->bundleManager->purgeFiles($server, $dryRun);

            $this->io->progressAdvance();
        }

        $this->io->progressFinish();

        $dryRun ?
            $this->io->note('Dry-run deployment has ended.') :
            $this->io->success(
                'Deployment was successful in '
                . $this->getWatchReadableDuration($this->getName())
            );

        return 0;
    }

    private function promptServerChoice(OutputInterface $output, array $servers)
    {
        $table = new Table($output);
        $table->setHeaderTitle('Configured servers (' . count($servers) . ')');
        $table->setHeaders(
            [
                '#',
                'Name',
                'Instance Id',
                'Server Id',
                'IP',
                'Webserver ?',
            ]
        );

        $table->addRow([0, self::ALL_SERVERS]);

        foreach ($servers as $serverNumber => $server) {
            $table->addRow([
                $serverNumber,
                $server->getName(),
                $server->getInstanceId(),
                $server->getServerId(),
                $server->getIp(),
                $server->hasWebServer() ? 'Yes' : 'No',
            ]);
        }

        $table->render();

        return $this->io->choice(
            'Select server number to deploy (0 for all)',
            array_merge(
                [self::ALL_SERVERS],
                array_map(
                    fn($server) => $server->getName(), $servers
                ),
            ),
            self::ALL_SERVERS
        );
    }
}
