<?php

namespace Ox\Console;

use Exception;
use Ox\Config\DeployConfig;
use Ox\Entity\Report;
use Ox\Service\BundleManager;
use Ox\Service\ERPManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Exception\ExceptionInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class AutoUpdateCommand
 *
 * @package Ox\Console
 */
class AutoUpdateCommand extends Command
{
    use LockableTrait;

    protected static $defaultName = 'auto-update';

    private BundleManager $bundleManager;
    private DeployConfig  $config;
    private ERPManager    $erpManager;
    private Report        $report;
    private SymfonyStyle  $io;

    public function __construct(DeployConfig $config, BundleManager $bundleManager, ERPManager $erpManager)
    {
        parent::__construct();
        $this->config        = $config;
        $this->bundleManager = $bundleManager;
        $this->erpManager    = $erpManager;
        $this->report        = Report::createFromConfig($this->config);
    }

    protected function configure(): void
    {
        $this->setDescription('Run auto update process in a non-interactive mode');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws Exception
     * @throws TransportExceptionInterface|DecodingExceptionInterface|ExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io = new SymfonyStyle($input, $output);

        $directory = $this->config->getMasterDirectory();
        if (!$this->lock($directory)) {
            $this->io->caution(
                [
                    $this->getName() . ' command is already running on directory ' . $directory,
                    'This command will consequently be skipped.',
                ]
            );
            return 1;
        }

        try {
            $this->bundleManager->checkCurrentRelease(true, $this->report);
            $this->config->setAuth($this->erpManager->getAutoUpdateAuth());

            $this->report->addContent(
                'ERP Auto-update Authorization : '
                . json_encode($this->config->getAuth())
            );

            if ($this->config->isUpdateAllowed()) {
                $this->io->title('ERP Update authorization granted. Running auto update...');
                $this->executeAutoInstall($input, $output);
            } else {
                $this->io->note('ERP has not granted the authorization to update. Skipping auto update...');
            }

            $this->bundleManager->getServersStatuses($this->report);

            $this->io->title('Sending auto-update report to ERP...');
            if (!$this->erpManager->sendReport($this->report)) {
                $this->io->error('Sending auto-update report to ERP has failed.');
            } else {
                $this->io->success('Auto-update report successfully sent to ERP.');
            }
        } catch (Exception $e) {
            $this->report->setStatus(Report::STATUS_ERROR);
            if (!$this->erpManager->sendReport($this->report, $e)) {
                $this->io->error('Sending auto-update report to ERP has failed.');
            } else {
                $this->io->error($e->getMessage());
                $this->io->success('Auto-update report successfully sent to ERP.');
            }
            return 1;
        }

        return 0;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws Exception
     * @throws ExceptionInterface
     */
    protected function executeAutoInstall(InputInterface $input, OutputInterface $output): int
    {
        $commands = [
            'sync'   => [
                '--auto'  => true,
            ],
            'build'  => [],
            'deploy' => [
                'instances' => $this->config->getAuthorizedIds()
            ],
        ];

        foreach ($commands as $command => $arguments) {
            $exitCode = $this->getApplication()->find($command)->run(
                new ArrayInput($arguments),
                $output
            );
            if ($exitCode !== 0) {
                $this->report->addContent('Command ' . $command . ' has failed with exit code : ' . $exitCode);
                $this->report->setStatus(Report::STATUS_ERROR);
                return $exitCode;
            }
        }

        return 0;
    }
}
