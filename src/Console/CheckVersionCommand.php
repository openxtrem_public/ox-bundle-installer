<?php

namespace Ox\Console;

use Composer\Semver\Comparator as SemverComparator;
use Composer\Semver\Semver;
use Exception;
use Ox\Config\DeployConfig;
use Packagist\Api\Client as PackagistApiClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class CheckVersionCommand
 *
 * @package Ox\Console
 */
class CheckVersionCommand extends Command
{
    protected static $defaultName = 'check-version';

    private DeployConfig $config;

    public function __construct(DeployConfig $config)
    {
        parent::__construct();
        $this->config = $config;
    }

    protected function configure(): void
    {
        $this->setDescription('Checks current client version against the latest available version from the registry')
            ->setDefinition(
                new InputDefinition([
                    new InputArgument(
                        'name',
                        InputArgument::OPTIONAL,
                        'The package name on the registry (default is the application name)'
                    ),
                    new InputArgument(
                        'version',
                        InputArgument::OPTIONAL,
                        'The version (default is the application current version)'
                    ),
                ])
            );
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $name =           $input->getArgument('name') ?? $this->getApplication()->getName();
        $currentVersion = $input->getArgument('version') ?? $this->getApplication()->getVersion();

        $versions = (new PackagistApiClient())->get($name)->getVersions();

        foreach ($versions as $version) {

            $versionString = $version->getVersion();

            if (!Semver::satisfies($versionString, 'v*.*')) {
                continue;
            }

            if (SemverComparator::greaterThan($versionString, $currentVersion)) {
                $io->error(
                    "Application " . $name . ' must be updated to the latest available version '
                    . $versionString . " (current is " . $currentVersion . ")" . PHP_EOL .
                    "Run the command `composer run update-project` to perform the update."
                );

                return 1;
            }
        }

        $io->success(
            'Application ' . $name . ' is currently running the latest available version ' . $currentVersion . '.'
        );

        return 0;
    }
}
