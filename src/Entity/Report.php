<?php

namespace Ox\Entity;

use Carbon\Carbon;
use JsonSerializable;
use Ox\Config\DeployConfig;

final class Report implements JsonSerializable
{
    public const INSTANCE_OK = 'ok';
    public const INSTANCE_KO       = 'ko';
    public const INSTANCE_NA       = 'na';
    public const INSTANCE_OUTDATED = 'outdated';

    public const STATUS_ERROR = 0;
    public const STATUS_OK      = 1;
    public const STATUS_WARNING = 2;

    protected const STATUS_TYPES = [
        self::STATUS_ERROR,
        self::STATUS_OK,
        self::STATUS_WARNING
    ];

    private string $identifier;
    private string $date;
    private ?int $serverId;
    private int $status;
    private float $duration = 0.0;
    private string $releaseCode;
    private string $body = '';

    private array $instances = [
        self::INSTANCE_OK       => [],
        self::INSTANCE_KO       => [],
        self::INSTANCE_NA       => [],
        self::INSTANCE_OUTDATED => [],
    ];

    public function __construct(
        string  $identifier,
        string  $date,
        ?int    $serverId,
        int     $status
    ) {
        $this->identifier  = $identifier;
        $this->date        = $date;
        $this->serverId    = $serverId;
        $this->status      = $status;
    }

    public static function createFromConfig(DeployConfig $config): self
    {
        return new self(
            self::makeIdentifier($config),
            Carbon::now(),
            $config->getMasterServerId(),
            self::STATUS_OK
        );
    }

    public function setReleaseCode(string $releaseCode): string
    {
        $this->addContent('Release code : ' . $releaseCode);
        return $this->releaseCode = $releaseCode;
    }

    public function setStatus(int $status): void
    {
        if (in_array($status, self::STATUS_TYPES)) {
            $this->status = $status;
        }
    }

    public function updateDuration(): float
    {
        return $this->duration = round(
            Carbon::now()->floatDiffInSeconds($this->date),
            2
        );
    }

    public function registerServer(Server $server, ?array $status, bool $updateAllowed): void
    {
        $type = self::INSTANCE_NA;
        if ($server->hasWebServer()) {
            /* The server status if not empty, contains outdated data */
            if (is_array($status)) {
                $type = self::INSTANCE_OK;
                if (!empty($status)) {
                    $type = self::INSTANCE_OUTDATED;
                    $this->addContent(json_encode($status));
                }
            } else {
                $type = self::INSTANCE_KO;
            }
        }

        $serverData = [
            'instance_id'    => $server->getInstanceId(),
            'server_id'      => $server->getServerId(),
            'update_allowed' => $updateAllowed,
        ];

        $this->instances[$type][] = $serverData;

        $this->addContent('Server info : ' .json_encode(array_merge(['status' => $type], $serverData)));
        if ($type === self::INSTANCE_OUTDATED) {
            $this->addContent('Outdated contents : ' .json_encode($status));
        }
    }

    public function addContent(string $content): string
    {
        return $this->body .= (!empty($this->body) ? PHP_EOL : '') . $content;
    }

    private static function makeIdentifier(DeployConfig $config): string
    {
        return $config->getMasterServerId() . '-' . $config->getMasterName();
    }

    public function jsonSerialize(): array
    {
        $replacementKeys = [
            'id'           => 'identifier',
            'server_id'    => 'serverId',
            'branch'       => 'releaseCode',
            'elapsed_time' => 'duration',
        ];

        $instancesReplacementKeys = [
            'instance_ids_ok'          => self::INSTANCE_OK,
            'instance_ids_ko'          => self::INSTANCE_KO,
            'instance_ids_unavailable' => self::INSTANCE_NA,
            'instance_ids_outdated'    => self::INSTANCE_OUTDATED,
        ];

        $vars = get_object_vars($this);

        foreach ($vars as $key => $var) {
            if (in_array($key, $replacementKeys)) {
                $vars[array_search($key, $replacementKeys)] = $var;
                unset($vars[$key]);
            }
        }

        if (array_key_exists('instances', $vars)) {
            foreach ($vars['instances'] as $instancesKey => $instances) {
                if (in_array($instancesKey, $instancesReplacementKeys)) {
                    $vars[array_search($instancesKey, $instancesReplacementKeys)] = $instances;
                }
            }
            unset($vars['instances']);
        }

        return $vars;
    }
}
