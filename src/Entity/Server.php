<?php

namespace Ox\Entity;

final class Server
{
    protected const LOCALHOST     = 'localhost';
    protected const HTTP_PROTOCOL = 'http://';

    private string $name;
    private int $instanceId;
    private int    $serverId;
    private string  $path;
    private ?string $webPath = null;
    private string $ipAddress;
    private string $token;
    private bool   $hasWebserver = true;
    private string $targetUrl;

    public function __construct(
        string $name,
        int $instanceId,
        int $serverId,
        string $path,
        ?string $webPath,
        string $ipAddress,
        string $token,
        bool $hasWebserver
    ) {
        $this->name         = $name;
        $this->instanceId   = $instanceId;
        $this->serverId     = $serverId;
        $this->path         = $path;
        $this->webPath      = $webPath;
        $this->ipAddress    = $ipAddress;
        $this->token        = $token;
        $this->hasWebserver = $hasWebserver;
        $this->targetUrl    = $this->makeTargetUrl();
    }

    /**
     * Creates and returns an array of Server entities
     *
     * @param array $config
     *
     * @return array
     */
    public static function createFromConfig(array $config): array
    {
        $configuredServers = $config['instance']['servers'] ?? [];

        $servers      = [];
        $serverNumber = 0;

        foreach ($configuredServers as $s) {
            $serverNumber++;

            $ip = self::LOCALHOST;
            if (preg_match('/(?:[0-9]{1,3}\.){3}[0-9]{1,3}/', $s['path'], $match)) {
                $ip = $match[0];
            }

            $servers[$serverNumber] = new self(
                $s['shortname'] ?? null,
                intval($s['instance_id'] ?? null),
                intval($s['server_id'] ?? null),
                $s['path'] ?? null,
                $s['web_path'] ?? null,
                $ip,
                $config['instance']['token_hash'],
                empty($s['no_webserver'] ?? null),
            );
        }

        return $servers;
    }

    public static function extractInstancesIds(array $servers): array
    {
        $data = [];
        foreach ($servers as $server) {
            if (!($server instanceof self)) {
                continue;
            }
            $data[] = $server->getInstanceId();
        }

        return array_unique($data);
    }

    public static function filterByInstances(array $servers, array $ids): array
    {
        $result = [];
        foreach ($servers as $server) {
            if (in_array($server->getInstanceId(), $ids)) {
                $result[] = $server;
            }
        }
        return $result;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getInstanceId(): int
    {
        return $this->instanceId;
    }

    public function getServerId(): int
    {
        return $this->serverId;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getWebPath(): ?string
    {
        return $this->webPath;
    }

    public function getIp(): string
    {
        return $this->ipAddress;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function hasWebServer(): bool
    {
        return $this->hasWebserver;
    }

    public function getAutoUpdate(): bool
    {
        return $this->autoupdate;
    }

    public function setAutoUpdate(bool $autoupdate): void
    {
        $this->autoupdate = $autoupdate;
    }

    public function getTargetUrl(): string
    {
        return $this->targetUrl;
    }

    public function makeTargetUrl(): string
    {
        return self::HTTP_PROTOCOL
            . $this->ipAddress . '/'
            . (!empty($this->webPath) ? trim($this->webPath) : basename($this->path));
    }
}
