<?php

namespace Ox\Entity;

use Carbon\Carbon;

final class Bundle
{
    private string $uuid;
    private int     $instance_id;
    private ?string $label;
    private ?bool   $active;
    private ?string $path;
    private ?bool   $integrity;
    private ?string $branch;
    private ?string $last_sync_date;

    public function __construct(
        string $uuid,
        int $instance_id,
        ?string $label = '',
        ?bool $active = false,
        ?string $path = null,
        ?bool $integrity = false,
        ?string $branch = null,
        ?string $last_sync_date = null
    ) {
        $this->uuid           = $uuid;
        $this->instance_id    = $instance_id;
        $this->label          = $label;
        $this->active         = $active;
        $this->path           = $path;
        $this->integrity      = $integrity;
        $this->branch         = $branch;
        $this->last_sync_date = $last_sync_date;
    }

    /**
     * @param array $data
     *
     * @return self|null
     */
    public static function createFromJSON(array $data): ?self
    {
        if (array_key_exists('data', $data) && ($data['data'] > 0)) {
            $item = $data['data']['attributes'];
            return new self(
                $item['ox_deploy_bundle_uuid'],
                $item['instance_id'],
                $item['label'],
                $item['active'],
                $item['path'],
                $item['integrity'],
                $item['branch'],
                $item['last_sync_date'],
            );
        };

        return null;
    }

    /**
     * @param array $data
     *
     * @return self[]
     */
    public static function createArrayFromJSON(array $data): ?array
    {
        if (array_key_exists('data', $data) && ($data['data'] > 0)) {
            $bundles = [];
            foreach ($data['data'] as $item) {
                $bundles[] = new self(
                    $item['attributes']['ox_deploy_bundle_uuid'],
                    $item['attributes']['instance_id'],
                    $item['attributes']['label'],
                    $item['attributes']['active'],
                );
            }
            return $bundles;
        };

        return null;
    }

    public function getUUID(): ?string
    {
        return $this->uuid;
    }

    public function getInstanceId(): ?int
    {
        return $this->instance_id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function getIntegrity(): bool
    {
        return $this->integrity;
    }

    public function getBranch(): ?string
    {
        return $this->branch;
    }

    public function getLastSyncDate(): ?string
    {
        return $this->last_sync_date;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function __toString(): string
    {
        return $this->uuid . ($this->label ? (' (' . $this->label . ')') : '');
    }

    public function getLastSyncRelativeDate(): ?string
    {
        return Carbon::createFromDate($this->last_sync_date)->diffForHumans();
    }
}
