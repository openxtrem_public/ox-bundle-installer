<?php

namespace Ox\Service;

use Ox\Config\DeployConfig;
use Ox\Entity\Bundle;
use Ox\Entity\Report;
use Ox\Entity\Server;
use RuntimeException;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\ConsoleSectionOutput;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Process\Process;
use Symfony\Component\Yaml\Yaml;
use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class BundleManager
 * @package Ox\Service
 */
class BundleManager
{
    protected const BUNDLE_INSTALLER_YML_PATH = 'bundle-installer.yml';
    protected const BUNDLE_YML_PATH           = 'bundle.yml';

    private const API_MONITORING_STATUS       = '/api/monitorClient/modules/status';

    private HttpClientInterface $client;
    private Filesystem $filesystem;
    private string $masterDirectory;
    private ConsoleSectionOutput $output;
    public DeployConfig $config;

    public function __construct(DeployConfig $config)
    {
        $this->config          = $config;
        $this->filesystem      = new Filesystem();
        $this->masterDirectory = $this->config->getMasterDirectory();
        $this->output          = (new ConsoleOutput())->section();

        $this->client = HttpClient::create(
            [
                'headers' => [
                    'X-OXAPI-KEY' => $this->config->getInstanceToken(),
                    'ox-devtools' => 2,
                ]
            ]
        );
    }

    public function runBundleProcess(array $cmd, string $dir = null, bool $silent = false): Process
    {


        $ps = Process::fromShellCommandline(implode(' ', $cmd), $dir, null, null, 600);

        if (true === $silent) {
            $ps->run(function ($type, $buffer) {});
        } else {
            $ps->run(function ($type, $buffer) {
                $this->output->overwrite($buffer);
            });
        }
        return $ps;
    }

    public function build(): void
    {
        $yml = Yaml::parseFile($this->checkInstallScriptPath());

        $buildCommands = $yml['build'];
        if (empty($buildCommands)) {
            throw new RuntimeException('Build failed. No build commands were found.');
        }

        foreach ($buildCommands as $command) {
            $ps = $this->runBundleProcess(
                explode(' ', $command),
                $this->masterDirectory
            );
            if (!$ps->isSuccessful()) {
                throw new RuntimeException(
                    'Build failed running : ' . $ps->getCommandLine()
                    . ' (Exit Code : ' . $ps->getExitCode() . ')'. PHP_EOL
                    . $ps->getErrorOutput()
                );
            }
        }
    }

    public function sync(Bundle $bundle): void
    {
        if (!$this->filesystem->exists($this->masterDirectory)) {
            $this->filesystem->mkdir($this->masterDirectory);
        }

        $ps = $this->runBundleProcess($this->makeSyncCommand($bundle));
        if (!$ps->isSuccessful()) {
            throw new RuntimeException('Synchronization failed. ' . $ps->getExitCode() . ' : ' .$ps->getErrorOutput());
        }

        $this->makeStatusFile($bundle);
    }

    public function deploy(Server $server, bool $dryRun = false): void
    {
        $ps = $this->runBundleProcess(
            $this->makeDeployCommand($server, $dryRun)
        );

        if (!$ps->isSuccessful()) {
            throw new RuntimeException(
                'Deployment failed to server '
                . $server->getName()
                . ' with path ' . $server->getPath()
                . ' (' . $ps->getExitCode() . ') running command : ' . $ps->getCommandLine()
                . ' : '
                . $ps->getErrorOutput()
            );
        } elseif ($dryRun) {
            $this->output->overwrite(
                $ps->getOutput() ?: (PHP_EOL . 'Nothing to copy to server ' . $server->getName() . '.')
            );
        }
    }

    public function purge(Server $server, bool $dryRun = false): void
    {
        $yml = Yaml::parseFile($this->checkInstallScriptPath());
        $purgeablePathList = $yml['deploy']['rsync']['purge'] ?? null;
        if (!empty($purgeablePathList)) {
            foreach ($purgeablePathList as $purgeablePath) {

                $masterPath = $this->config->getMasterDirectory() . '/' . $purgeablePath;

                if (!$this->filesystem->exists($masterPath)) {
                    $this->filesystem->mkdir($masterPath);
                }

                $ps = $this->runBundleProcess(
                    $this->makePurgeCommand($server, $purgeablePath, $dryRun),
                    null,
                    true
                );

                if ($this->filesystem->exists($masterPath)) {
                    $this->filesystem->remove($masterPath);
                }

                if (!$ps->isSuccessful()) {
                    $error = $ps->getErrorOutput();
                    /* During the first deployment, it is likely that folders don't exist on the destination */
                    if ((null !== $error) && str_contains($error, 'No such file or directory')) {
                        $this->output->overwrite(
                            '<comment>'
                            . 'Notice : ' . $purgeablePath . ' cannot be purged because target directory does not exist yet.'
                            . '</comment>'
                        );
                        return;
                    }

                    throw new RuntimeException(
                        'Purge failed to server '
                        . $server->getName()
                        . ' with path ' . $server->getPath()
                        . ' (' . $ps->getExitCode() . ') running command : ' . $ps->getCommandLine()
                        . ' : '
                        . $ps->getErrorOutput()
                    );
                }
            }
        }
    }


    public function purgeFiles(Server $server, bool $dryRun = false): void
    {
        $yml = Yaml::parseFile($this->checkInstallScriptPath());
        $purgeablePathList = $yml['deploy']['rsync']['purgeFiles'] ?? null;

        if (!empty($purgeablePathList)) {
            foreach ($purgeablePathList as $purgeablePath) {

                $ps = $this->runBundleProcess(
                    $this->makePurgeFilesCommand($server, $purgeablePath, $dryRun),
                    null,
                    true
                );

                if (!$ps->isSuccessful()) {
                    throw new RuntimeException(
                        'Purging files failed to server '
                        . $server->getName()
                        . ' with path ' . $server->getPath()
                        . ' (' . $ps->getExitCode() . ') running command : ' . $ps->getCommandLine()
                        . ' : '
                        . $ps->getErrorOutput()
                    );
                }
            }
        }
    }

    public function hasCurrentData(): bool
    {
        return $this->filesystem->exists($this->getStatusFilePath());
    }

    private function getCurrentData(string $field): ?string
    {
        if ($this->filesystem->exists($this->getStatusFilePath())) {
            $yml = Yaml::parseFile($this->getStatusFilePath());
            return array_key_exists($field, $yml) ? $yml[$field] : null;
        }

        return null;
    }

    public function getCurrentReleaseCode(): ?string
    {
        return $this->getCurrentData('release_code');
    }

    public function getCurrentUniqueIdentifier(): ?string
    {
        return $this->getCurrentData('uuid');
    }

    public function getCurrentLatestUpdateDate(): ?string
    {
        return $this->getCurrentData('last_update');
    }

    public function getServerStatus(Server $server): ?array
    {
        try {
            $url = $server->getTargetUrl() . self::API_MONITORING_STATUS;
            $response = $this->client->request(
                'GET',
                $url
            );

            if ($response->getStatusCode() === 200) {
                return $response->toArray();
            }

            $this->output->write(
                [
                    '<error>',
                    'Error while requesting instance status : ' . $url,
                    $response->getStatusCode() . ' : ' . $response->getContent(),
                    'Check that URL and access token are properly configured.',
                    '</error>'
                ]
            );

            return null;
        } catch (ExceptionInterface $e) {
            $this->output->write(
                [
                    '<error>',
                    'Error while requesting instance status : ' . $url,
                    $e->getMessage(),
                    'Check that URL and access token are properly configured.',
                    '</error>'
                ]
            );

            return null;
        }
    }

    public function getServersStatuses(?Report $report = null): void
    {
        foreach ($this->config->getServers() as $server) {
            $status = null;
            if ($server->hasWebServer()) {
                $status = $this->getServerStatus($server);
            }

            if ($report instanceof Report) {
                $report->registerServer(
                    $server,
                    $status,
                    $this->config->isUpdateAllowed(
                        $server->getInstanceId()
                    )
                );
            }
        }
    }

    public function checkCurrentRelease(bool $throw = false, ?Report $report = null): void
    {
        $releaseCode = $this->getCurrentReleaseCode();
        if (!empty($releaseCode) && ($report instanceof Report)) {
            $report->setReleaseCode($releaseCode);
        } elseif ($throw) {
            throw new RuntimeException(
                'Master directory current branch is unknown. '.
                'Auto-update cannot be performed.',
            );
        }
    }

    private function makeBundleWebPath(Bundle $bundle): string
    {
        return str_replace($this->config->getDeployPrefix(), '', $bundle->getPath());
    }

    /**
     * @param Bundle $bundle
     *
     * @return array
     * @throws RuntimeException
     */
    private function makeSyncCommand(Bundle $bundle): array
    {
        switch ($this->config->getDeployTransportMethod()) {
            case 'rclone':
                return [
                    'rclone',
                    'sync',
                    '--stats-one-line',
                    '--progress',
                    '--create-empty-src-dirs',
                    '--skip-links',
                    '--http-url',
                    $this->config->makeDeployWebUrl(),
                    ':http:' . $this->makeBundleWebPath($bundle),
                    $this->config->getMasterDirectory(),
                ];
            case 'rsync':
                $cmd = [
                    'rsync',
                    '-azPL',
                    '--quiet',
                    '--stats',
                    '--delete',
                ];

                $excludedFilesList = array_merge(
                    $this->config->getRsyncProtections(),
                    $this->config->getRsyncExclusions()
                );

                foreach ($excludedFilesList as $exclude) {
                    $cmd[] = '--exclude=' . $exclude;
                }

                return array_merge(
                    $cmd,
                    [
                        $this->config->getDeployUser() . '@' . $this->config->getDeployUrl() . ':' . $bundle->getPath() . '/',
                        $this->config->getMasterDirectory() .'/'
                    ]
                );

            default:
                throw new RuntimeException('Transport method is invalid.');
        }
    }

    /**
     * @param Server $server
     * @param bool   $dryRun
     *
     * @return array
     */
    private function makeDeployCommand(Server $server, bool $dryRun = false): array
    {
        $yml = Yaml::parseFile($this->checkInstallScriptPath());
        $excludedFilesDefaultList = $yml['deploy']['rsync']['exclude'];
        if (empty($excludedFilesDefaultList)) {
            throw new RuntimeException(
                'Deploy failed. '
                . 'The list of rsync excluded files was not found in '
                . $this->masterDirectory
            );
        }

        $excludedFilesList = array_merge(
            $excludedFilesDefaultList,
            $this->config->getRsyncExclusions()
        );

        $command = [
            "rsync",
            "-rltzD",
            "--delete",
        ];

        foreach ($excludedFilesList as $exclude) {
            $command[] = '--exclude=' . $exclude;
        }

        if (true === $dryRun) {
            $command[] = '-ni';
        }

        return array_merge(
            $command,
            [
                $this->config->getMasterDirectory() . '/',
                $server->getPath(),
            ]
        );
    }

    /**
     * @param Server $server
     * @param string $path
     * @param bool   $dryRun
     *
     * @return array
     */
    private function makePurgeCommand(Server $server, string $path, bool $dryRun = false): ?array
    {
        $command = [
            'rsync',
            '-rm',
            '--delete',
        ];

        if (true === $dryRun) {
            $command[] = '-ni';
        }

        return array_merge(
            $command,
            [
                $this->config->getMasterDirectory() . '/' . $path . '/',
                $server->getPath() . '/' . $path,
            ]
        );
    }

    /**
     * @param Server $server
     * @param string $path
     * @param bool   $dryRun
     *
     * @return array
     */
    private function makePurgeFilesCommand(Server $server, string $path, bool $dryRun = false): ?array
    {
        $paths   = explode('/', $path);
        $pattern = array_pop($paths);
        $dir     = implode('/', $paths);

        $command = [
            'rsync',
            '-r',
            '--delete',
        ];

        if (true === $dryRun) {
            $command[] = '-ni';
        }

        return array_merge(
            $command,
            [
                '--include="' . $pattern . '"',
                '--exclude="*"',
                $this->config->getMasterDirectory() . '/' . $dir . '/',
                $server->getPath() . '/' . $dir,
            ]
        );
    }

    public function makeStatusFile(Bundle $bundle): void
    {
        $path = $this->getStatusFilePath();
        if ($this->filesystem->exists($path)) {
            $this->filesystem->remove($path);
        }

        $this->filesystem->dumpFile(
            $path,
            Yaml::dump(
                [
                    'uuid'         => $bundle->getUUID(),
                    'release_code' => $bundle->getBranch(),
                    'last_update'  => date("Y-m-d H:i:s")
                ]
            )
        );
    }

    public function getStatusFilePath(): string
    {
        return $this->masterDirectory . '/' . self::BUNDLE_YML_PATH;
    }

    public function checkInstallScriptPath(): ?string
    {
        $path = $this->getInstallScriptPath();

        if (!$this->filesystem->exists($path)) {
            throw new FileNotFoundException(
                ' Configuration file within master directory is missing : '
                . $path
                . '. Consider running the "sync" or "install" command again.'
            );
        }
        return $path;
    }

    public function getInstallScriptPath(): string
    {
        return $this->masterDirectory . '/' . self::BUNDLE_INSTALLER_YML_PATH;
    }
}
