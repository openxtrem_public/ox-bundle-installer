<?php

namespace Ox\Service;

use Exception;
use Ox\Config\DeployConfig;
use Ox\Entity\Report;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class ERPManager
 * @package Ox\Service
 */
class ERPManager
{
    private const API_BUNDLES_PATH         = '/api/oxDeploy/bundles';
    private const API_AUTO_UPDATE_AUTH     = '/api/monitorServer/autoupdate/auth';
    private const API_AUTO_UPDATE_REGISTER = '/api/monitorServer/autoupdate/register';

    private string $url;
    private HttpClientInterface $client;
    private DeployConfig $config;

    public function __construct(DeployConfig $config)
    {
        $this->config = $config;
        $this->url    = $this->config->getErpUrl();
        $this->client = HttpClient::create(
            [
                'headers' => [
                    'X-OXAPI-KEY' => $this->config->getErpToken(),
                    'ox-devtools' => 2,
                ]
            ]
        );
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     */
    public function listBundles(): array
    {
        $response = $this->client->request(
            'GET',
            $this->url . self::API_BUNDLES_PATH
        );
        if ($response->getStatusCode() === 200) {
            return $response->toArray();
        } else {
            throw new TransportException('Data could not be retrieved from ERP API : ' . $response->getStatusCode());
        }
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function getBundle(string $uuid): array
    {
        $response = $this->client->request(
            'GET',
            $this->url . self::API_BUNDLES_PATH . '/' . $uuid
        );

        switch ($response->getStatusCode()) {
            case 200:
                return $response->toArray();
            case 401:
                throw new TransportException('ERP API : Unauthorized, check authorization token is valid.');
            case 403:
                throw new TransportException('ERP API : Forbidden, check authorization token is valid.');
            case 404:
                throw new TransportException('ERP API : Bundle was not found.');
            default:
                throw new TransportException('ERP API : Data could not be retrieved. (HTTP ' . $response->getStatusCode() . ')');
        }
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function markBundleSynchronized(string $uuid): bool
    {
        $response = $this->client->request(
            'POST',
            $this->url . self::API_BUNDLES_PATH . '/' . $uuid . '/notify'
        );

        return $response->getStatusCode() === 200;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     */
    public function getAutoUpdateAuth(): ?array
    {
        $response = $this->client->request(
            'POST',
            $this->url . self::API_AUTO_UPDATE_AUTH,
            [
                'json' => [
                    'data' => $this->config->getInstancesIdentifiers()
                ]
            ]
        );

        if ($response->getStatusCode() === 200) {
            return $response->toArray();
        }

        throw new TransportException('ERP Auto-update auth data could not be retrieved.');
    }

    /**
     * @throws Exception
     * @throws TransportExceptionInterface|DecodingExceptionInterface
     */
    public function sendReport(Report $report, Exception $e = null): ?array
    {
        if ($e instanceof Exception) {
            $report->addContent($e->getMessage());
        }

        $report->updateDuration();

        $response = $this->client->request(
            'POST',
            $this->url . self::API_AUTO_UPDATE_REGISTER,
            [
                'json' => [
                    'data' => $report
                ]
            ]
        );

        if ($response->getStatusCode() === 200) {
            return $response->toArray();
        }

        return null;
    }
}
