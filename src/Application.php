<?php

namespace Ox;

use Symfony\Component\Console\Application as SymfonyApplication;

class Application extends SymfonyApplication
{
    private const PACKAGE_NAME    = 'openxtrem/ox-bundle-installer';
    public const PACKAGE_VERSION = 'v2.0.3';

    public function __construct(iterable $commands)
    {
        parent::__construct();

        $this->setName(self::PACKAGE_NAME);
        $this->setVersion(self::PACKAGE_VERSION);

        foreach ($commands as $command) {
            $this->add($command);
        }
    }
}