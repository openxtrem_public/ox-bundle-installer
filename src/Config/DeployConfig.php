<?php

namespace Ox\Config;

use Ox\Entity\Server;
use RuntimeException;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Yaml\Yaml;

/**
 * Class DeployConfig
 * @package Ox\Config
 */
class DeployConfig
{
    private const VALIDATION_YML  = 'config/validator/validation.yml';
    private const DEPLOY_FILEPATH = 'config/parameters.yml';

    protected const UPDATE_ALLOWED_CODES = [1, 3];

    private array $servers = [];
    private array $instancesIdentifiers = [];
    private string $bundleUniqueId;
    private string $masterDirectory;
    private string $masterName;
    private int $masterServerId;
    private string $erpUrl;
    private string $erpToken;
    private string $deployUrl;
    private string $deployProtocol;
    private string $deployPrefix;
    private string $deployUser;
    private string $deployToken;
    private string $deployTransportMethod;
    private string $instanceToken;
    private Filesystem $filesystem;
    private array $rsyncExclusions = [];
    private array $rsyncProtections = [];
    private array $auth = [];

    public function __construct(?array $params = null) {
        $this->filesystem = new Filesystem();
        $this->loadParameters($params);
    }

    public function getViolationsList(?string $validationFilePath = null): ConstraintViolationList
    {
        return Validation::createValidatorBuilder()
            ->addYamlMapping($validationFilePath ?: self::VALIDATION_YML)
            ->getValidator()->validate($this);
    }

    public function loadParameters(?array $params): void
    {
        $params = $params ?: $this->parseParametersFile();

        $this->bundleUniqueId        = $params['bundle']['uuid'] ?? null;
        $this->masterDirectory       = $params['master']['path'] ?? null;
        $this->masterName            = $params['master']['name'] ?? null;
        $this->masterServerId        = $params['master']['server_id'] ?? null;
        $this->erpUrl                = $params['auth']['erp']['url'] ?? null;
        $this->erpToken              = $params['auth']['erp']['api_token'] ?? null;
        $this->deployUrl             = $params['auth']['oxdeploy']['url'] ?? null;
        $this->deployProtocol        = $params['auth']['oxdeploy']['protocol'] ?? null;
        $this->deployUser            = $params['auth']['oxdeploy']['http_user'] ?? null;
        $this->deployToken           = $params['auth']['oxdeploy']['http_token'] ?? null;
        $this->deployPrefix          = $params['auth']['oxdeploy']['prefix'] ?? null;
        $this->deployTransportMethod = $params['auth']['oxdeploy']['transport_method'] ?? null;
        $this->instanceToken         = $params['instance']['token_hash'] ?? null;
        $this->rsyncExclusions       = $params['deploy']['rsync']['exclude'] ?? [];
        $this->rsyncProtections      = $params['deploy']['rsync']['protect'] ?? [];

        $this->servers              = Server::createFromConfig($params);
        $this->instancesIdentifiers = Server::extractInstancesIds($this->servers);
    }

    public function parseParametersFile(?string $path = null): array
    {
        $path = $path ?? self::DEPLOY_FILEPATH;

        if (!$this->filesystem->exists($path)) {
            throw new FileNotFoundException('Configuration file ' . $path . ' is missing.');
        }

        return Yaml::parseFile($path);
    }

    public function getBundleUniqueId(): string
    {
        return $this->bundleUniqueId;
    }

    public function getServers(): ?array
    {
        return $this->servers;
    }

    public function getInstancesIdentifiers(): array
    {
        return $this->instancesIdentifiers;
    }

    public function getErpUrl(): string
    {
        return $this->erpUrl;
    }

    public function getErpToken(): string
    {
        return $this->erpToken;
    }

    public function getDeployUrl(): string
    {
        return $this->deployUrl;
    }

    public function getDeployProtocol(): string
    {
        return $this->deployProtocol;
    }

    public function getDeployUser(): string
    {
        return $this->deployUser;
    }

    public function getDeployToken(): string
    {
        return $this->deployToken;
    }

    public function getDeployPrefix(): string
    {
        return $this->deployPrefix;
    }

    public function getDeployTransportMethod(): string
    {
        return $this->deployTransportMethod;
    }

    public function getMasterDirectory(): string
    {
        return $this->masterDirectory;
    }

    public function getMasterName(): string
    {
        return $this->masterName;
    }

    public function getMasterServerId(): ?int
    {
        return $this->masterServerId;
    }

    public function getInstanceToken(): string
    {
        return $this->instanceToken;
    }

    public function getRsyncExclusions(): array
    {
        return $this->rsyncExclusions;
    }

    public function getRsyncProtections(): array
    {
        return $this->rsyncProtections;
    }

    public function getAuth(): array
    {
        return $this->auth;
    }

    public function setAuth(array $auth): array
    {
        return $this->auth = $auth;
    }

    public function makeDeployWebUrl(): string
    {
        return $this->getDeployProtocol() . '://' . $this->getDeployUser() . ':' . $this->getDeployToken()
            . '@' . $this->getDeployUrl();
    }

    public function isUpdateAllowed(?int $instanceId = null): bool
    {
        if ($instanceId) {
            return in_array($instanceId, $this->getAuthorizedIds());
        }

        return !empty($this->getAuthorizedIds());
    }

    public function getAuthorizedIds(): array
    {
        return array_keys(
            array_filter(
                $this->getAuth(),
                fn($authCode) => in_array($authCode, self::UPDATE_ALLOWED_CODES)
            )
        );
    }

    public function checkBinaryRequirements(): bool
    {
        $commands = [
            [
                "cmd"  => 'composer',
                "msg"  => "Composer is not installed on system.",
                "help" => "https://getcomposer.org"
            ],
            [
                "cmd"  => 'npm',
                "msg"  => "NPM is not installed on system.",
                "help" => "https://www.npmjs.com/get-npm"
            ],
            [
                "cmd"  => 'rsync',
                "msg"  => "Rsync is not installed on system.",
                "help" => "https://doc.ubuntu-fr.org/rsync"
            ],
            [
                "cmd"  => 'rclone',
                "msg"  => "Rclone binary is not installed on system.",
                "help" => "https://rclone.org/install/"
            ]
        ];

        foreach ($commands as $command) {
            if (!is_executable(trim(shell_exec('command -v ' . $command["cmd"])))) {
                throw new RuntimeException($command['msg'] . ' : ' . $command['help']);
            }
        }

        return true;
    }
}
