# ox-bundle-installer

## Description

A command application written in PHP to manage OX Mediboard software installations.

## Installation

```
composer create-project openxtrem/ox-bundle-installer
```

## Usage

```
bin/console [command] [arguments]
```

### Available commands:

| Command | Description |
|---|---|
|**auto-update**|Run auto update process in a non-interactive mode|
|**browse**|Browse and display available bundles|
|**build**|Build bundle dependencies and assets|
|**config**|Load and validate configuration file|
|**deploy**|Deploy bundle from master directory to target servers directories|
|**help**|Display help for a command|
|**install**|Run sync, then build and finally deploy commands|
|**list**|List commands|
|**sync**|Synchronize bundle contents to local master directory|

## Binary Requirements

| Binary | Version |
|---|---|
|**php**|7.3|
|**composer**|2.0|
|**rsync**|3.1.3|
|**rclone**|1.56.0|

> The application **config** command allows to check those requirements.


## Configuration

Create a configuration file, in the `config` directory named `parameters.yml`.

Use the `config/parameters.yml-dist` file as a template.

### Remote authentication

#### OX-ERP

Authentication on OX-ERP is required to get information and statuses on bundles that are to be installed.

```
auth:
  erp:
    url: https://sample-erp.openxtrem.com
    api_token: IdkIfThisIsGood
```

#### OX-Deploy

OX-Deploy is the alias for the server hosting the bundles.

Authentication on OX-Deploy is required to download the bundle contents locally.

```
  oxdeploy:
    url: sample.openxtrem.com
    protocol: https
    http_user: user
    http_token: ToKeN
    prefix: /var/www/deployment
    transport_method: rclone
```

Two transport methods are available : `rclone` and `rsync`.

> Parameters `protocol`, `http_user`, `http_token` are only required when using the `rclone` transport method.
> 
> The `rsync` method requires that the local server has access to the OX-Deploy server through the SSH protocol.

### Bundle

A bundle is identified by its unique id.

```
bundle:
  uuid: abcd1234-ef56-gh78-ij90-klmno12345
```

> Though this parameter is not required to manually install a bundle, it is mandatory to be set so automatic updates can be performed.

### Targets

```
master:
  path: /var/master
  name: prod
  server_id: 1
instance:
  name: INSTANCE_NAME
  token_hash: 27dab2832d4568dfa8b60ab389398b893405f543
  servers:
    -
      path: "/var/path_to/instance/a"
      shortname: "Instance A"
      id: 44
      server_id: 1
      web_path: "path_to/instance/a"
      no_webserver: yes
    -
      path: "user@host:/var/path_to/instance/b"
      shortname: "Instance B"
      id: 44
      server_id: 2
      web_path: "path_to/instance/b"
```

#### Master

The `master:path` is the local path where bundles contents from the OX-Deploy server will be copied to.

> This directory **will be created locally if it does not exist yet** and if permissions allow to do so.

#### Instance

The instance `token_hash` is an authentication token that must be valid to retrieve the instance status (valid, outdated, broken) from the webservers.

#### Servers

At least on server must be configured. Plus, every configured path **must** exist or be created manually (for both local and remote targets).

## Development and testing

Set up a docker container for development using :

```
docker-compose up -d
docker exec -it ox-bundle-installer-app-1 bash
```

> Project sources are mounted with a volume under `/var/www/html` inside the container.

Inside the container :

```
# Install dependencies
composer install
```

### Running tests

```
# Run tests
XDEBUG_MODE=coverage vendor/bin/phpunit
```