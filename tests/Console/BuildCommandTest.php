<?php

namespace Ox\Tests\Console;

use Ox\Console\BuildCommand;
use Ox\Tests\Utils\BundleManagerMock;
use Ox\Tests\Utils\DeployConfigMock;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

class BuildCommandTest extends TestCase
{
    public function testBuildWithoutMasterDirectory()
    {
        $this->expectException(FileNotFoundException::class);
        $this->expectExceptionMessage('Configuration file within master directory is missing');

        $application = new Application();
        $application->add(
            new BuildCommand(
                DeployConfigMock::getValid(),
                BundleManagerMock::get()
            )
        );
        $command = $application->find('build');
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);
    }

}