<?php

namespace Ox\Tests\Console;

use Ox\Config\DeployConfig;
use Ox\Console\AutoUpdateCommand;
use Ox\Service\BundleManager;
use Ox\Tests\Utils\BundleManagerMock;
use Ox\Tests\Utils\DeployConfigMock;
use Ox\Tests\Utils\ERPManagerMock;
use PHPUnit\Framework\MockObject\MockBuilder;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class AutoUpdateCommandTest extends TestCase
{
    /** @var MockBuilder|DeployConfig */
    private $config;

    /** @var MockBuilder|BundleManager */
    private $manager;

    public function setUp(): void
    {
        $this->config  = DeployConfigMock::getValid();
        $this->manager = BundleManagerMock::get();

        $fs = new Filesystem();
        if (!$fs->exists($this->config->getMasterDirectory())) {
            $fs->mkdir($this->config->getMasterDirectory());
        }

        $fs->copy(
            __DIR__ . '/../Resources/master/bundle-valid.yml',
            $this->manager->getStatusFilePath()
        );
    }

    public function tearDown(): void
    {
        $fs = new Filesystem();
        if ($fs->exists($this->config->getMasterDirectory())) {
            $fs->remove($this->manager->getStatusFilePath());
        }
    }

    /**
     * @runInSeparateProcess
     * @dataProvider autoUpdateCommandProvider
     */
    public function testExecute($data, $expected)
    {
        $application = new Application();
        $application->add(
            new AutoUpdateCommand(
                $this->config,
                $this->manager,
                ERPManagerMock::get(true, $data['auth'])
            )
        );

        $this->assertFileExists($this->manager->getStatusFilePath());

        $command = $application->find('auto-update');
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);

        $this->assertStringContainsString(
            $expected,
            $commandTester->getDisplay()
        );
    }

    public function autoUpdateCommandProvider(): array
    {
        return [
            'auth_allowed' => [
                [
                    'auth' => true
                ],
                'Running auto update'
            ],
            'auth_denied' => [
                [
                    'auth' => false
                ],
                'ERP has not granted the authorization to update'
            ]
        ];
    }
}
