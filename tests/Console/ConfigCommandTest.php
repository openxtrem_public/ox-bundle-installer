<?php

namespace Ox\Tests\Console;

use Ox\Console\ConfigCommand;
use Ox\Tests\Utils\DeployConfigMock;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class ConfigCommandTest extends TestCase
{
    /**
     * @dataProvider configCommandProvider
     */
    public function testExecute($config, $expected)
    {
        $application = new Application();
        $application->add(new ConfigCommand($config));
        $command = $application->find('config');
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);

        $this->assertStringContainsString(
            $expected,
            $commandTester->getDisplay()
        );
    }

    public function configCommandProvider(): array{
        return [
            'valid' => [
                DeployConfigMock::getValid(),
                'Configuration loaded and validated successfully'
            ],
            'invalid' => [
                DeployConfigMock::getInvalid(),
                'Configuration is invalid.'
            ]
        ];
    }
}
