<?php

namespace Ox\Tests\Console;

use Ox\Console\DeployCommand;
use Ox\Tests\Utils\BundleManagerMock;
use Ox\Tests\Utils\DeployConfigMock;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

class DeployCommandTest extends TestCase
{
    public function testDeployWithoutMasterDirectory()
    {
        $this->expectException(FileNotFoundException::class);
        $this->expectExceptionMessage('Configuration file within master directory is missing');

        $application = new Application();
        $application->add(
            new DeployCommand(
                DeployConfigMock::getValid(),
                BundleManagerMock::get()
            )
        );
        $command = $application->find('deploy');
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);
    }

}