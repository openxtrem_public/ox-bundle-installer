<?php

namespace Ox\Tests\Console;

use Ox\Console\SyncCommand;
use Ox\Tests\Utils\BundleManagerMock;
use Ox\Tests\Utils\DeployConfigMock;
use Ox\Tests\Utils\ERPManagerMock;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class SyncCommandTest extends TestCase
{
    /** @dataProvider syncCommandProvider */
    public function testExecute($data, $expected)
    {
        $application = new Application();
        $application->add(
            new SyncCommand(
                DeployConfigMock::getValid(),
                BundleManagerMock::get(),
                ERPManagerMock::get($data['integrity']),
            )
        );
        $command = $application->find('sync');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            '--auto' => true
        ]);

        $this->assertStringContainsString(
            $expected,
            $commandTester->getDisplay()
        );
    }

    public function syncCommandProvider(): array{
        return [
            'valid' => [
                [
                    'integrity' => true
                ],
                'synchronized successfully'
            ],
            'invalid' => [
                [
                    'integrity' => false
                ],
                'integrity is compromised'
            ]
        ];
    }
}
