<?php

namespace Ox\Tests\Console;

use Ox\Console\InstallCommand;
use Ox\Tests\Utils\BundleManagerMock;
use Ox\Tests\Utils\DeployConfigMock;
use Ox\Tests\Utils\ERPManagerMock;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class InstallCommandTest extends TestCase
{
    /**
     * @runInSeparateProcess
     * @dataProvider installCommandProvider
     */
    public function testExecute($data, $expected)
    {
        $commands = [
            new InstallCommand(
                DeployConfigMock::getValid(),
                BundleManagerMock::get(),
                ERPManagerMock::get(true, false, $data['report'])
            )
        ];
        $application = new Application();
        $application->addCommands($commands);
        $command = $application->find('install');
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);

        $this->assertStringContainsString(
            $expected,
            $commandTester->getDisplay()
        );
    }

    public function installCommandProvider(): array
    {
        return [
            'report_sent' => [
                [
                    'report' => true
                ],
                'Auto-update report successfully sent to ERP'
            ],
            'report_not_sent' => [
                [
                    'report' => false
                ],
                'Sending auto-update report to ERP has failed'
            ]
        ];
    }
}
