<?php

namespace Ox\Tests\Console;

use Ox\Console\BrowseCommand;
use Ox\Tests\Utils\BundleManagerMock;
use Ox\Tests\Utils\DeployConfigMock;
use Ox\Tests\Utils\ERPManagerMock;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class BrowseCommandTest extends TestCase
{
    public function testExecute()
    {
        $application = new Application();
        $application->add(
            new BrowseCommand(
                DeployConfigMock::getValid(),
                BundleManagerMock::get(),
                ERPManagerMock::get(),
            )
        );
        $command = $application->find('browse');
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);

        $this->assertStringContainsString(
            'Bundle Sample Title',
            $commandTester->getDisplay()
        );
    }
}
