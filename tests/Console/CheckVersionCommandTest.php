<?php

namespace Ox\Tests\Console;

use Ox\Console\CheckVersionCommand;
use Ox\Tests\Utils\DeployConfigMock;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class CheckVersionCommandTest extends TestCase
{
    /**
    * @dataProvider checkVersionCommandProvider
    */
    public function testExecute($data, $expected)
    {
        $application = new Application();
        $application->add(new CheckVersionCommand(DeployConfigMock::getValid()));
        $command = $application->find('check-version');
        $commandTester = new CommandTester($command);
        $commandTester->execute($data);
        $this->assertEquals(
            $expected,
            $commandTester->getStatusCode()
        );
    }

    public function checkVersionCommandProvider(): array
    {
        return [
            'ok' => [
                [
                    'name'    => 'openxtrem/ox-bundle-installer',
                    'version' => \Ox\Application::PACKAGE_VERSION
                ],
                0
            ],
            'outdated' => [
                [
                    'name'    => 'openxtrem/ox-bundle-installer',
                    'version' => 'v0.0.1'
                ],
                1
            ],
        ];
    }
}