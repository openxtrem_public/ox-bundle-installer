<?php

namespace Ox\Tests\Console;

use Ox\Console\StatusCommand;
use Ox\Tests\Utils\BundleManagerMock;
use Ox\Tests\Utils\DeployConfigMock;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class StatusCommandTest extends TestCase
{
    public function testExecute()
    {
        $application = new Application();
        $application->add(
            new StatusCommand(
                DeployConfigMock::getValid(),
                BundleManagerMock::get()
            )
        );
        $command = $application->find('status');
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);

        $this->assertStringContainsString(
            'Current bundle data file is missing',
            $commandTester->getDisplay()
        );
    }
}
