<?php

namespace Ox\Tests\Service;

use Ox\Config\DeployConfig;
use Ox\Service\ERPManager;
use Ox\Tests\Utils\DeployConfigMock;
use Ox\Tests\Utils\ERPManagerMock;
use PHPUnit\Framework\MockObject\MockBuilder;
use PHPUnit\Framework\TestCase;
use Ox\Entity\Report;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class ERPManagerTest extends TestCase
{
    /** @var MockBuilder|DeployConfig */
    private $config;

    /** @var MockBuilder|ERPManager */
    private $manager;

    public function setUp(): void
    {
        $this->config  = DeployConfigMock::getValid();
        $this->manager = new ERPManager($this->config);
    }

    public function tearDown(): void
    {
        $this->manager = null;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testShouldReturnAPIData()
    {
        $this->manager = ERPManagerMock::get();

        $this->assertIsArray($this->manager->getBundle('db5da999-0b91-42f6-a9e5-6701c465f7b2'));
        $this->assertIsArray($this->manager->listBundles());
        $this->assertArrayHasKey('data', $this->manager->listBundles());
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testFailedListBundles()
    {
        $this->expectException(TransportException::class);
        $this->manager->listBundles();
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testFailedGetBundle()
    {
        $this->expectException(TransportException::class);
        $this->manager->getBundle($this->config->getBundleUniqueId());
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testFailedMarkBundleSynchronized()
    {
        $this->assertFalse($this->manager->markBundleSynchronized($this->config->getBundleUniqueId()));
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testFailedGetAutoUpdateAuth()
    {
        $this->expectException(TransportException::class);
        $this->manager->getAutoUpdateAuth();
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testFailedSendReport()
    {
        $this->assertNull($this->manager->sendReport(Report::createFromConfig($this->config)));
    }
}
