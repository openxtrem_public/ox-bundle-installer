<?php

namespace Ox\Tests\Service;

use Ox\Config\DeployConfig;
use Ox\Entity\Bundle;
use Ox\Service\BundleManager;
use Ox\Tests\Utils\BundleManagerMock;
use Ox\Tests\Utils\DeployConfigMock;
use PHPUnit\Framework\MockObject\MockBuilder;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use Symfony\Component\Filesystem\Filesystem;

class BundleManagerTest extends TestCase
{
    /** @var MockBuilder|DeployConfig */
    private $config;

    /** @var MockBuilder|BundleManager */
    private $manager;

    /** @var Bundle */
    private $bundle;

    public function setUp(): void
    {
        $this->config  = DeployConfigMock::getValid();
        $this->manager = BundleManagerMock::get();
        $this->bundle  = Bundle::createFromJSON(
            json_decode(
                file_get_contents(__DIR__ . './../Resources/api/oxDeploy/bundle_integrity_ok.json'), true
            )
        );
    }

    public function tearDown(): void
    {
        $fs = new Filesystem();
        if ($fs->exists($this->config->getMasterDirectory())) {
            $fs->remove($this->manager->getInstallScriptPath());
            $fs->remove($this->manager->getStatusFilePath());
        }

        $this->manager = null;
    }

    public function testSyncWasSuccessful()
    {
        $this->manager->sync($this->bundle);
        $this->assertFileExists($this->manager->getStatusFilePath());
    }

    public function testDeploySuccess()
    {
        $this->preparePostSyncEnv();
        $this->expectOutputString('');

        $servers = $this->config->getServers();
        $this->manager->deploy(reset($servers));
    }

    public function testDeployFailure()
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Deployment failed');

        $this->manager = BundleManagerMock::get(false);

        $this->preparePostSyncEnv();

        $servers = $this->config->getServers();
        $this->manager->deploy(reset($servers));
    }

    public function testPurgeSuccess()
    {
        $this->preparePostSyncEnv();
        $this->expectOutputString('');

        $servers = $this->config->getServers();
        $this->manager->purge(reset($servers));
    }

    public function testPurgeFailure()
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Purge failed');

        $this->manager = BundleManagerMock::get(false);

        $this->preparePostSyncEnv();

        $servers = $this->config->getServers();
        $this->manager->purge(reset($servers));
    }

    public function testBuildSuccess()
    {
        $this->preparePostSyncEnv();
        $this->expectOutputString('');

        $this->manager->build();
    }

    public function testBuildFailureOnConfiguration()
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Build failed');

        $this->preparePostSyncEnv(false);

        $this->manager->build();
    }

    public function testBuildFailureOnExecution()
    {
        $this->manager = BundleManagerMock::get(false);

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Build failed');

        $this->preparePostSyncEnv();

        $this->manager->build();
    }

    private function preparePostSyncEnv(bool $valid = true) {
        $fs = new Filesystem();
        if (!$fs->exists($this->config->getMasterDirectory())) {
            $fs->mkdir($this->config->getMasterDirectory());
        }

        $fs->copy(
            __DIR__ . '/../Resources/master/bundle-installer-'
            . ($valid ? 'valid' : 'invalid') . '.yml',
            $this->manager->getInstallScriptPath()
        );
    }
}
