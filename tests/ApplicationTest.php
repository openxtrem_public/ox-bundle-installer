<?php

namespace Ox\Tests;

use Ox\Application;
use Ox\Console\ConfigCommand;
use Ox\Tests\Utils\DeployConfigMock;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Tester\ApplicationTester;

class ApplicationTest extends TestCase
{
    public function testRun()
    {
        $application = new Application([
            new ConfigCommand(DeployConfigMock::getValid())
        ]);
        $application->setAutoExit(false);
        $applicationTester = new ApplicationTester($application);
        $exitCode          = $applicationTester->run(['config']);
        $this->assertEquals(0, $exitCode);
    }
}