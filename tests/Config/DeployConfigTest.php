<?php

namespace Ox\Tests\Config;

use Ox\Tests\Utils\DeployConfigMock;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Yaml\Yaml;

class DeployConfigTest extends TestCase
{
    public function setUp(): void
    {
    }

    public function tearDown(): void
    {
    }

    /**
     * @dataProvider configurationDataProvider
     */
    public function testConfiguration(array $data, array $expected): void
    {
        $config = DeployConfigMock::get($data);

        $this->assertTrue($config->checkBinaryRequirements());
        $this->assertEquals(
            $expected['violations_count'],
            $config->getViolationsList(__DIR__ . '/../../config/validator/validation.yml')->count()
        );
    }

    public function testParsingFailed()
    {
        $this->expectException(FileNotFoundException::class);

        DeployConfigMock::getValid()->parseParametersFile('invalid/path/parameters.yml');
    }

    public function testParsingSuccess()
    {
        $config = DeployConfigMock::getValid();

        $this->assertIsArray($config->parseParametersFile(__DIR__ . '/../Resources/config/valid.yml'));
        $this->assertMatchesRegularExpression(
            '/^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/i',
            $config->getBundleUniqueId()
        );
        $this->assertStringContainsString('http', $config->getDeployProtocol());
        $this->assertIsString($config->getDeployUrl());
        $this->assertIsString($config->getDeployUser());
        $this->assertIsString($config->getDeployToken());
        $this->assertIsString($config->getDeployPrefix());
        $this->assertIsString($config->getMasterDirectory());
        $this->assertIsString($config->getMasterName());
        $this->assertGreaterThan(0, $config->getMasterServerId());
        $this->assertCount(1, $config->getServers());
        $this->assertCount(1, $config->getInstancesIdentifiers());
        $this->assertFalse($config->isUpdateAllowed());
        $this->assertIsString($config->getInstanceToken());
        $this->assertNotEmpty($config->getRsyncExclusions());
        $this->assertNotEmpty($config->getRsyncProtections());
        $this->assertEmpty($config->getAuth());
    }

    public function configurationDataProvider(): array
    {
        return [
            'valid' => [
                Yaml::parse(file_get_contents(__DIR__ . '/../Resources/config/valid.yml')),
                [
                    'violations_count' => 0
                ]
            ],
            'invalid_single' => [
                Yaml::parse(file_get_contents(__DIR__ . '/../Resources/config/invalid_single_violation.yml')),
                [
                    'violations_count' => 1
                ]
            ],
            'invalid_multiple' => [
                Yaml::parse(file_get_contents(__DIR__ . '/../Resources/config/invalid_multiple_violations.yml')),
                [
                    'violations_count' => 6
                ]
            ]
        ];
    }

}