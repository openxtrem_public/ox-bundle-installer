<?php

namespace Ox\Tests\Utils;

use Ox\Config\DeployConfig;
use Ox\Service\ERPManager;
use PHPUnit\Framework\MockObject\Generator;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Yaml\Yaml;

abstract class ERPManagerMock
{
    /* @return MockObject|ERPManager */
    public static function get(
        bool $integrity = true,
        bool $allowed = false,
        bool $report = true
    ): MockObject {

        $mockedMethods = [
            'getAutoUpdateAuth',
            'getBundle',
            'listBundles',
        ];

        if ($report) {
            $mockedMethods[] = 'sendReport';
        }

        $mock = (new Generator())->getMock(
            ERPManager::class,
            $mockedMethods,
            [DeployConfigMock::getValid()]
        );

        $getBundleResponse = json_decode(
            file_get_contents(
                __DIR__ . './../Resources/api/oxDeploy/bundle_integrity_' . ($integrity ? 'ok' : 'ko') . '.json'
            ),
            true
        );

        $getAuthResponse = json_decode(
            file_get_contents(__DIR__ . './../Resources/api/monitorServer/auth_' . ($allowed ? 'allowed' : 'denied') .'.json'), true
        );

        $listBundlesResponse = json_decode(
            file_get_contents(__DIR__ . './../Resources/api/oxDeploy/bundle_list.json'), true
        );

        $sendReportResponse = json_decode(
            file_get_contents(__DIR__ . './../Resources/api/monitorServer/report.json'), true
        );

        $mock->method('getAutoUpdateAuth')->willReturn($getAuthResponse);
        $mock->method('getBundle')->willReturn($getBundleResponse);
        $mock->method('listBundles')->willReturn($listBundlesResponse);

        if ($report) {
            $mock->method('sendReport')->willReturn($sendReportResponse);
        }

        return $mock;
    }
}
