<?php

namespace Ox\Tests\Utils;

use Ox\Config\DeployConfig;
use PHPUnit\Framework\MockObject\Generator;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Yaml\Yaml;

abstract class DeployConfigMock
{
    /* @return MockObject|DeployConfig */
    public static function get(array $params): MockObject
    {
        $mock = (new Generator())->getMock(
            DeployConfig::class,
            ['checkBinaryRequirements'],
            [$params]
        );

        $mock->method('checkBinaryRequirements')->willReturn(true);

        return $mock;
    }

    /* @return MockObject|DeployConfig */
    public static function getValid(): MockObject
    {
        return self::get(
            Yaml::parse(
                file_get_contents(__DIR__ . './../Resources/config/valid.yml')
            )
        );
    }

    /* @return MockObject|DeployConfig */
    public static function getInvalid(): MockObject
    {
        return self::get(
            Yaml::parse(
                file_get_contents(__DIR__ . './../Resources/config/invalid_single_violation.yml')
            )
        );
    }
}
