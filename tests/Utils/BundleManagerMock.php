<?php

namespace Ox\Tests\Utils;

use Ox\Service\BundleManager;
use PHPUnit\Framework\MockObject\Generator;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Process\Process;

abstract class BundleManagerMock
{
    /* @return MockObject|BundleManager */
    public static function get(bool $successful = true): MockObject
    {
        $mock = (new Generator())->getMock(
            BundleManager::class,
            [
                'runBundleProcess',
                'getServerStatus'
            ],
            [DeployConfigMock::getValid()],
        );

        $mock->method('runBundleProcess')->willReturn(
            self::getProcess($successful)
        );
        $mock->method('getServerStatus')->willReturn([]);

        return $mock;
    }

    public static function getProcess(bool $successful = true): MockObject
    {
        $mock = (new Generator())->getMock(
            Process::class,
            [
                'setTimeout', 'run', 'wait', 'stop', 'getCommandLine',
                'getExitCode', 'getExitCodeText', 'getWorkingDirectory',
                'isOutputDisabled', 'getErrorOutput', 'isSuccessful', 'getOutput'
            ],
            [
                [
                    'command line to execute'
                ],
            ],
        );

        $mock->method('isSuccessful')->willReturn($successful);
        $mock->method('getOutput')->willReturn(
            'Mocked process execution : ' . ($successful ? 'Success' : 'Failure')
        );

        return $mock;
    }
}
