#!/usr/bin/env sh

SRC=$1
DEST=$2
ID=$3

# Create a dummy structure of files before deployment
mkdir -p "$DEST/tmp/templates_c" "$DEST/files/private" "$DEST/cli/conf"
touch "$DEST/tmp/templates_c/purged.tpl" "$DEST/tmp/CAccessLog.buffer" "$DEST/tmp/CAccessLog.buffertemp"
touch "$DEST/tmp/application.log" "$DEST/files/private/file.txt" "$DEST/includes/config.php" "$DEST/cli/conf/deploy.xml"

# Silently perform deployment
bin/console deploy instances "$ID"

# Test file in a purge directory has been deleted
FILE="$DEST/tmp/templates_c/purged.tpl"
if test -f "$FILE" ; then
   echo "Error: file $FILE should have been deleted" && exit 1
else
  echo "Success: file $FILE has been deleted"
fi

# Test files not in a purge directory were left intact
FILE="$DEST/tmp/application.log"
if test -f "$FILE" ; then
  echo "Success: file $FILE was left intact"
else
   echo "Error: file $FILE should have been kept" && exit 1
fi

# Test files in a purge files pattern have been deleted
FILE="$DEST/tmp/CAccessLog.buffer"
if test -f "$FILE" ; then
   echo "Error: file $FILE should have been deleted" && exit 1
else
  echo "Success: file $FILE has been deleted"
fi

FILE="$DEST/tmp/CAccessLog.buffertemp"
if test -f "$FILE" ; then
   echo "Error: file $FILE should have been deleted" && exit 1
else
  echo "Success: file $FILE has been deleted"
fi

FILE="$DEST/files/private/file.txt"
if test -f "$FILE" ; then
  echo "Success: file $FILE was left intact"
else
   echo "Error: file $FILE should have been kept" && exit 1
fi

FILE="$DEST/cli/conf/deploy.xml"
if test -f "$FILE" ; then
  echo "Success: file $FILE was left intact"
else
   echo "Error: file $FILE should have been kept" && exit 1
fi

# Clean dummy structure after execution
rm "$DEST/tmp/application.log" "$DEST/files/private/file.txt" "$DEST/tmp/CAccessLog.buffer*"
rm -r "$DEST/tmp/templates_c" "$DEST/cli/conf" "$DEST/modules/monitorClient/cli/conf"

exit 0